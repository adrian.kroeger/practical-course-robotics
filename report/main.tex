\documentclass[
	12pt,
	%German, % Uncomment for German
]{style}

% Template-specific packages
\usepackage[utf8]{inputenc} % Required for inputting international characters
\usepackage[T1]{fontenc} % Output font encoding for international characters
\usepackage[parfill]{parskip}
\usepackage{hyperref}
\usepackage[nameinlink]{cleveref}
\usepackage{mathpazo} % Use the Palatino font
\usepackage{graphicx} % Required for including images
\usepackage{booktabs} % Required for better horizontal rules in tables
\usepackage{listings} % Required for insertion of code
\usepackage{enumerate} % To modify the enumerate environment
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{array}
\usepackage[cache=false]{minted}
\setminted[python]{linenos, breaklines, fontsize=\footnotesize}



%-------------------------------------------------------
%	ASSIGNMENT INFORMATION
%-------------------------------------------------------

\title{Maze Solving Robot} % Assignment title

\authorOne{Adrian Kröger, 21872150} % Student name
\authorTwo{Darius Lewen, 21867430} % Student name
\authorThree{Tim Dietrich, 21865883} % Student name

\date{6th April 2021} % Due date

\institute{Georg-August-Universität Göttingen \\ Drittes Physikalisches Institut} % Institute or school name

\class{Practical Course on Computer Vision \& Robotics} % Course or class name

\professor{Sebastian Ruiz, Florian Teich} % Professor or teacher in charge of the assignment

%-------------------------------------------------------



\begin{document}

\maketitle

%-------------------------------------------------------
%	ASSIGNMENT CONTENT
%-------------------------------------------------------

\section{Method}

From the beginning it was clear to us to use the A* algorithm for solving the maze, because it is one of the fastest and most reliable algorithms for this task and on top of that it was also easy to implement. \\
As a heuristic to balance between exploration and exploitation we chose to explore the space until we encountered two red blocks, or if there is just one way left to the goal. During this exploration we still drive along the current shortest path to the goal.\\
We determined the red block we wanted to push by the distance of said block to the goal, the idea being that this would be the best decision with our knowledge at the time.\\
\\
Regarding the construction, we mounted the light array in front of the axle to detect a crossing before the robot overshot it.\\
To detect red and blue blocks we put the ultrasonic sensor in front of the robot, but as close to the brick as possible. \\
Then for determining the color of a block we put the color sensor in front of the ultrasonic sensor, but facing down, a bit like a scorpions tail. The intention here was to not touch the block but still being able to approach it closely. \\
We varied the height of the contraption and concluded that about a centimeter above a block was the best height to detect colors. But even then we encountered the problem that a blue block was sometimes being classified as green or black, but ultimately we had to put up with this.\\
When playing around with the driving speed we noticed that the robots center of gravity was causing it to almost tip over when braking. We addressed this issue by constructing an underbody for the robot and filling it with coins to lower the center of gravity.

\section{Discussion}

The most prominent problem that stretched over months, was trying to fine-tune the PID-algorithm to get decent results at following the line. At some point we implemented our own line-following which then worked out of the box. \\
A problem that arose after switching from micropython to python3, as we wanted to use numpy libraries, was the total loading time of all imported packages used in our code. Sometimes we had to wait for about a minute before the robot was moving. \\
Unanimously we agreed to drop numpy and recoded the algorithms to be working with micropython, which cut the loading time 10-fold.\\
\\
The weather often played a big part in the performance of our line-following and crossing detection.\\
Everytime a cloud appeared on a sunny day, we had to recalibrate the sensors or else the robot would be all over the place.\\
\\
But there were also many positives during the project.\\
The code we wrote for pathfinding and solving the maze worked straightforward without many bug fixes.\\
We also created a jupyter notebook which simulated a run of the robot on a given maze, so we could test many of our algorithms in theory before eventually implementing it on the robot itself.

\section{Teamwork}

At the beginning we split the work based on personal interest and experience with the tasks. 
This proved to be a good decision as each member could focus on the tasks they were most familiar with, even if the workload was not divided equally.\\
The biggest problem was that one of our group members, Tim, is not resident in Göttingen so it was hard to include him in the practical side, but we were able to discuss with him the theoretical approaches and give updates about the state of our robot so that he nonetheless was integrated well.

\clearpage
\appendix
\section{Time Schedule}\label{appendix:time_schedule}

\begin{table}[h]
\begin{tabularx}{\linewidth}{@{}p{2cm}p{4cm}Xc>{\centering\arraybackslash}c@{}}
\toprule
Date                  & Group Member(s)        & Description of Work \\
\midrule
15/11/2020& Darius, Adrian& Assembled robot and initialized GitLab Repo \\
\addlinespace[0.5em]
20/11/2020& Darius, Adrian, Tim& Discussed workflow and read through ev3dev2 doc \\
\addlinespace[0.5em]
25/11/2020& Darius, Adrian& Started line-following and configured robot speed \\
\addlinespace[0.5em]
04/12/2020& Darius, Adrian& Added block and color detection and push structure to robot \\
\addlinespace[0.5em]
10/12/2020& Darius, Adrian& Implemented PID and refined block pushing \\
\addlinespace[0.5em]
16/12/2020& Darius, Adrian& Configured PID-parameters and first tries at navigating a maze \\
\addlinespace[0.5em]
14/01/2021& Darius, Adrian& Implemented Debug-mode and played with PID-params \\
\addlinespace[0.5em]
21/01/2021& Darius, Adrian, Tim& Discussed solve heuristic \\
\addlinespace[0.5em]
09/03/2021& Darius, Adrian& Revamped Line Following, implemented A* in micropython \\
\addlinespace[0.5em]
10/03/2021& Darius, Adrian, Tim& cleaned code, discussed report structure \\
\addlinespace[0.5em]
11/03/2021& Darius, Adrian& Tested block pushing with new line-following, added data structure to detect straights \\
\addlinespace[0.5em]
15/03/2021& Darius, Adrian& Implemented robot stopping on middle of straight, corrected maze reading \\
\addlinespace[0.5em]
17/03/2021&Darius, Adrian& Corrected error in instruction dict, added recalculation of path if block detected \\
\addlinespace[0.5em]
22/03/2021& Darius, Adrian& Fixed bugs in crossing detection and straight handling, experimented with driving speed and weight distribution \\
\addlinespace[0.5em]
24/03/2021& Darius, Adrian, Tim& Implemented first part of red/blue block heuristic in theory \\
\addlinespace[0.5em]
25/03/2021& Darius, Adrian, Tim& Finished red/blue block heuristic and transferred code to robot \\
\bottomrule
\end{tabularx}
\end{table}
TBC on next page
\newpage
\begin{table}[h]
	\begin{tabularx}{\linewidth}{@{}p{2cm}p{4cm}Xc>{\centering\arraybackslash}c@{}}
		\toprule
		Date                  & Group Member(s)        & Description of Work \\
		\midrule
		30/03/2021& Darius, Adrian, Tim& Tested solver on robot and fixed a few mistakes \\
		\addlinespace[0.5em]
		02/04/2021& Darius, Adrian&Fixed remaining bugs and added timing of runs \\
		\addlinespace[0.5em]
		05/04/2021& Darius, Adrian, Tim&Cleaned code, finalized report and discussed about exam \\
		\bottomrule
	\end{tabularx}
\end{table}

\clearpage
\section{Code}\label{appendix:code}

\subsection*{Filename: pathfinder.py}
\inputminted{python}{../robot/pathfinder.py}

\subsection*{Filename: main.py}
\inputminted{python}{../robot/main.py}

\end{document}
