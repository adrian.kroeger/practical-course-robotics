import time

from uheapq import heappop
from uheapq import heappush
import itertools


from math import sqrt as mathsqrt
inf = float('inf')

r = (0,1)
d = (1,0)
l = (0,-1)
u = (-1,0)

moves=[r,d,l,u]

two_moves_to_rotation ={

    #the first move is the direction from where we are

    #0 - forward
    #1 - right
    #2 - left

    #equals 
    (r,r):0,
    (d,d):0,
    (l,l):0,
    (u,u):0,
    #right
    (r,d):1,
    #(r,d):2,
    (r,u):2,
    #(r,u):1,
    #down
    (d,r):2,
    (d,l):1,
    #left
    #(l,d):1,
    #(l,u):2,
    (l,d):2,
    (l,u):1,
    #up
    (u,r):1,
    (u,l):2,
    #opposite
    (u,d):3,
    (d,u):3,
    (l,r):3,
    (r,l):3,
}

maze = None

def read_maze(loc='/home/robot/robot/maze.txt'):
    """
    converts an ascii maze to an environment data structure,
    that can be fed to A*.
    Also determines location of start, goal and straights
    """
    with open(loc) as f:
        maze = f.read().strip()
        start, goal = None, None

        # calculate any offset of x and y coords
        offset_y = 0
        offset_x = 0
        rows = maze.split('\n')
        for i, row in enumerate(rows):
            s = row.find('S')
            if s != -1:
                offset_y = ((len(rows)+1)%2+i%2)%2
                offset_x = s%2

                start = (len(rows)-i, s+1)
            g = row.find('G')
            if g != -1:
                goal = (len(rows)-i, g+1)
        
        size_y = len(rows)
        size_x = max([len(row) for row in rows])
                
        env = full((size_y, size_x), 1)
        # convert link to 0 and walls to 1
        for y in range(size_y-1, -1, -1):
            for x in range(len(rows[y])):
                env[size_y-y-1][x] = (rows[y][x] == ' ')*1
        
        env2 = full((size_y+2, size_x+2), 1)
        # add border around complete maze
        for i in range(1,size_y+1):
            for j in range(1,size_x+1):
                env2[i][j] = env[i-1][j-1]

        # extract location of straights
        straights = []
        for i in range(1+offset_y,len(env2)-1,2):
            for j in range(1+offset_x, len(env2[i])-1,2):
                if (env2[i][j]+env2[i+1][j]+env2[i-1][j] == 0 and env2[i][j-1]+env2[i][j+1] == 2) or (env2[i][j]+env2[i][j+1]+env2[i][j-1] == 0 and env2[i-1][j]+env2[i+1][j] == 2):
                    straights.append((i,j))

        return env2, straights, start, goal

    

def reconstruct_dict_path(start, goal, came_from):
    """
    creates the path list out of the came_from dict.
    """
    current = came_from[goal]
    if current == start:
        return []
    return [current] + reconstruct_dict_path(start, current, came_from)

def full(shape, val):
    """
    returns an array of given shape with val in every element
    """
    out = [[val for _ in range(shape[1])] for _ in range(shape[0])]
    return out

def euklid_dist_2d(p1, p2):
    """
    calculates euklidean distance between two points
    """
    return mathsqrt(pow(p1[0]-p2[0],2)+pow(p1[1]-p2[1],2))


def manhattan(p1, p2):
    """
    calculates L1 distance between two points
    """
    return abs(p1[0]-p2[0])+abs(p1[1]-p2[1])

def a_star(start, goal, env, heuristic=manhattan):
    """
    start: (x,y) tuple, 2d startpoint
    goal:  (x,y) tuple, 2d goalpoint
    env:   2d numpy matrix with zeroes and ones only which encode the maze.
           The borders of the matrix have to be ones. Ones encode non
           passable areas and zeroes passable areas.
    h:     A appropriate heuristic for the A* algorithm.

    implemented a_star with a heap.
    """

    rowlen = len(env[0])
    collen = len(env)
    shape = (collen, rowlen)
    
    h = lambda x,y:heuristic(x,y)
    if env[start[0]][start[1]] != 0:
        return None
    came_from = {}
    g_score=full(shape, inf)
    g_score[start[0]][start[1]] = 0
    f_score=full(shape, inf)
    f_score[start[0]][start[1]] = h(start, goal)
    discovered_nodes = []
    heappush(discovered_nodes, (f_score[start[0]][start[1]], start))
    while discovered_nodes:
        current = heappop(discovered_nodes)[1]
        if current == goal:
            path = reconstruct_dict_path(start, current, came_from) #todo
            path.reverse()
            return [start]+path+[goal]
        for x_shift, y_shift in moves:
            neighbor = (current[0]+x_shift, current[1]+y_shift)
            if not (neighbor[0] < 0 or neighbor[1] < 0 or env[neighbor[0]][neighbor[1]] != 0):
                tentative_g_score = g_score[current[0]][current[1]] + h(current, neighbor)
                if tentative_g_score < g_score[neighbor[0]][neighbor[1]]:
                    came_from[neighbor] = current
                    g_score[neighbor[0]][neighbor[1]] = tentative_g_score
                    f_score[neighbor[0]][neighbor[1]] = g_score[neighbor[0]][neighbor[1]] + h(neighbor, goal)
                    if neighbor not in [e[1] for e in discovered_nodes]:
                        heappush(discovered_nodes,(f_score[neighbor[0]][neighbor[1]],neighbor))

    
def get_instruction_dict(start, goal, env, start_ori=d):
    """
    The expected start oriantation is east.
    start_ori:
    0 - east
    1 - south
    2 - north
    3 - west
    
    0 - forward
    1 - right
    2 - left
    3 - backward (can only occur in the first node)
    """
    
    if start == goal:
        return {}, []

    path = a_star(start, goal, env)
    if not path:
        return None, path
    instruction_dict = {}
    for node in range(len(path)):
        if node == 0: #for the first node
            next_move = (path[node+1][0]-path[node][0],path[node+1][1]-path[node][1])
            instruction_dict[path[node]] = two_moves_to_rotation[start_ori, next_move]
        elif node == len(path)-1: #for the last node
            instruction_dict[path[node]] = 3#finish
        else: #standard case
            next_move = (path[node+1][0]-path[node][0],path[node+1][1]-path[node][1])
            prev_move = (path[node][0]-path[node-1][0],path[node][1]-path[node-1][1])
            instruction_dict[path[node]] = two_moves_to_rotation[prev_move,next_move]
    return instruction_dict, path
