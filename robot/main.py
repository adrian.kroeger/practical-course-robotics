#!/usr/bin/env micropython
from ev3dev2.sensor import INPUT_1, INPUT_2, INPUT_3, INPUT_4, Sensor
from ev3dev2.sensor.lego import UltrasonicSensor, ColorSensor
from ev3dev2.sound import Sound
from ev3dev2.motor import OUTPUT_A, OUTPUT_B, OUTPUT_C, OUTPUT_D, MoveTank, SpeedRPS
from ev3dev2.console import Console
from ev3dev2.button import Button

import time
import sys
import pathfinder as pf
import itertools

line_leader = Sensor(INPUT_1)
us_sensor   = UltrasonicSensor(INPUT_4)
col_sensor  = ColorSensor(INPUT_3)
drive       = MoveTank(OUTPUT_B, OUTPUT_C)
ctl         = Button()

t_crossing  = 30
base_default = 2
base_slow = 0.5
base = base_default

current_pos = None
last_pos = None
goal = None
glob_ori = 1

idle = 0

inf = float('inf')

col_sensor.mode  = ColorSensor.MODE_COL_COLOR
us_sensor.mode   = UltrasonicSensor.MODE_US_DIST_CM

def argmin(a):
    """
    returns index of minimum element in list
    """
    m = a[0]
    l = 0
    for i in range(1, len(a)):
        if a[i] < m:
            m = a[i]
            l = i
    return l

def rotate(direction):
    """
    rotates the robot and updates orientation
    """
    global glob_ori

    if direction == 2: #right
        drive.on_for_rotations(SpeedRPS(1.5), SpeedRPS(-1.5), 0.5)
        glob_ori = (glob_ori-1)%4
    elif direction == 1: #left
        drive.on_for_rotations(SpeedRPS(1.5), SpeedRPS(-1.5), -0.5)
        glob_ori = (glob_ori+1)%4
    elif direction == 3: #turn around
        drive.on_for_rotations(SpeedRPS(1.5), SpeedRPS(-1.5), 1)
        glob_ori = (glob_ori+2)%4


def get_direction(end, inst_dict=None, straights=None):
    """
    if a crossing is reached, this function is called in order to handle global
    variables and rotate the robot.
    """

    global current_pos
    global glob_ori

    if not inst_dict: #manual mode
        while True:
            if ctl.left:
                drive.on_for_rotations(SpeedRPS(1.5), SpeedRPS(-1.5), -0.5)
                glob_ori = (glob_ori+1)%4
            elif ctl.right:
                drive.on_for_rotations(SpeedRPS(1.5), SpeedRPS(-1.5), 0.5)
                glob_ori = (glob_ori-1)%4
            elif ctl.up:
                return
    else: #debug mode

        current_node = next(current_pos)
        if current_node == end:
            return False, True #strt_ahead, goal_reached
        else:
            global last_pos
            last_pos = current_node

        direction = inst_dict[current_node]

        if inst_dict[next(current_pos)] != 0: #take link encodings out of iterator
            print('Error: expected link')
            return
        
        #peak for next node in order to check wether the next node is a unseeable straigt.
        peek = next(current_pos)
        strt_ahead = peek in straights

        if strt_ahead:
            last_pos = peek
            if inst_dict[next(current_pos)] != 0:
                print('Error: expected link')
                return
        else:
            current_pos = itertools.chain([peek], current_pos)

        rotate(direction)
        
        #measure time
        global idle
        idl_start = time.time()
        ctl.wait_for_bump('right')
        idle += time.time()-idl_start

        return strt_ahead, False #strt_ahead, goal_reached


def cal():
    """
    calibrates light array, color sensor
    and calculates a threshold for crossings.

    Calibration order: white ground, black ground, white on color sensor, robot on middle of path
    """
    ctl.wait_for_bump('right')
    line_leader.command = 'CAL-WHITE' #white
    Sound().beep()
    ctl.wait_for_bump('right')
    line_leader.command = 'CAL-BLACK' #black
    Sound().beep()
    line_leader.mode = 'CAL'
    light_on_crossing = read_vals() #col sensor onto white ground
    ctl.wait_for_bump('right')
    col_sensor.calibrate_white()
    Sound().beep()
    
    ctl.wait_for_bump('right')
    light = read_vals() #on line

    # calculates right and left part when on line
    mean_left_0 = sum(light[0:5])/5
    mean_right_0 = sum(light[4:])/5
    print('left', mean_left_0)
    print('right', mean_right_0)
    Sound().beep()
    mean_left_1 = sum(light_on_crossing[0:5])/5
    mean_right_1 = sum(light_on_crossing[4:])/5
    print('left', mean_left_1)
    print('right', mean_right_1)
    Sound().beep()

    global t_crossing
    t_crossing = (mean_left_0+mean_left_1+mean_right_0+mean_right_1)/4

    with open('t_crossing', 'w') as f:
        f.write(str(t_crossing))


def ask_for_cal():
    """
    asks the user for calibration.
    up to skip, down to calibrate
    """
    Sound().beep()
    while True:
        if ctl.up:  #skip calibration
            with open('t_crossing', 'r') as f:
                global t_crossing
                t_crossing = float(f.read())
            break
        elif ctl.down: #calibrate
            cal()
            ctl.wait_for_bump('right')
            break


def read_vals():
    """
    reads the light values of light array
    """
    a = [0] * 8
    b = [0,7,1,6,2,5,3,4]
    for i in b:
        a[i] = line_leader.value(i)
    return a


def check_for_block():
    """
    checks if block is in robots way by checking if something is less than 6cm in front of robot
    and then checks the color of the object
    """
    dist = us_sensor.distance_centimeters

    if dist <= 6:
        color = col_sensor.color

        if color == 5:
            print('detected red block')
            return 1 #red
        else:
            print('detected blue block')
            return 2 #blue
    else:
        return 0 #drive on


def line_correction(light, correction_speed=base_default/-10):
    """
    correct position until the line is in the middle of the light array.
    """

    start_time = time.time()

    global base
    m = argmin(light)
    # if robot is not centered, drive backwards until centered again
    if m not in [3,4]:
        drive.off(brake=True)
        while m not in [3,4]:
            if m in [0,1,2]:
                drive.on(SpeedRPS(correction_speed), SpeedRPS(0))
            elif m in [5,6,7]:
                drive.on(SpeedRPS(0), SpeedRPS(correction_speed))
            light = read_vals()
            m = argmin(light)
        drive.on(SpeedRPS(base), SpeedRPS(base))

    end_time = time.time()

    return end_time - start_time

def check_for_crossing(light):
    """
    reads light array, takes the means of the left and right side in
    order to detect a crossing.
    """
    
    mean_left = sum(light[0:5])/5
    mean_right = sum(light[3:])/5

    if  mean_left < t_crossing and mean_right < t_crossing:
        return 3 #left and right
    elif mean_left < t_crossing:
        return 1 #left
    elif mean_right < t_crossing:
        return 2 #right
    return 0

def abfahrt(inst_dict, path, straights, end):
    """
    The main driving function of the robot. Drives the robot form the current position
    to the goal or returns if a block is deteced.
    """

    stop_time = inf
    slow_time = inf
    strt_ahead = None

    global base
    global current_pos
    global glob_ori

    if len(inst_dict) == 0:
        return None, None

    current_pos = iter(path)

    #align to start direction.
    strt_ahead, goal_reached = get_direction(end, inst_dict, straights)

    #set timer for slow driving or stopping if a straigt is ahead.
    if strt_ahead:
        stop_time = time.time() + 28/(base*17.3)
    else:
        slow_time = time.time() + 18/(base*17.3)

    if goal_reached:
        print('reached goal!')
        return 0, None #case, block_pos

    drive.on(SpeedRPS(base), SpeedRPS(base))

    detected_case = None
    block_pos = None

    while True: #Drive until a block is detected or the goal is reached.
        dist = us_sensor.distance_centimeters
        if dist <= 10 and base > 0:
            base = .5

        case = check_for_block()

        if case != 0: #block detected
            drive.off(brake=True)
            drive.on_for_rotations(SpeedRPS(-base), SpeedRPS(-base), 2/17.3)
            drive.on_for_rotations(SpeedRPS(1), SpeedRPS(-1), 1)
            glob_ori = (glob_ori+2)%4
            base = base_default

            if last_pos in straights:
                stop_time = time.time() + 12/(base_slow*17.3)
                
            else:
                stop_time = inf

            slow_time = time.time()
            drive.on(SpeedRPS(base), SpeedRPS(base))
            block_pos = next(current_pos)
            detected_case = case

        if time.time() > stop_time: #in case a straight is ahead.
            drive.off(brake=True)
            stop_time = inf
            Sound().beep()
            
            base = base_default

            if detected_case:
                return detected_case, block_pos

            global idle
            idl_start = time.time()
            ctl.wait_for_bump('right')
            idle += time.time()-idl_start
            slow_time = time.time() + 18/(base*17.3)
            drive.on(SpeedRPS(base), SpeedRPS(base))
        elif time.time() > slow_time: #drive slow in order to get better line detection
            base = base_slow
            slow_time = inf
            drive.on(SpeedRPS(base), SpeedRPS(base))

        light = read_vals()

        if check_for_crossing(light) == 0: #take line correction time and add it to correction/stop time
            correction_time = line_correction(light)
            stop_time += correction_time
            slow_time += correction_time
        else: #detected crossing
            print('detected crossing')
            base = base_default
            drive.on_for_rotations(SpeedRPS(1), SpeedRPS(1), 4/17.3, brake=True, block=True)
            Sound().beep() #dont move this
            if detected_case:
                return detected_case, block_pos
            strt_ahead, goal_reached = get_direction(end, inst_dict, straights)

            if goal_reached:
                return 0, None
            elif strt_ahead:
                stop_time = time.time() + 28/(base*17.3)
            else:
                slow_time = time.time() + 18/(base*17.3)

            drive.on(SpeedRPS(base), SpeedRPS(base))
                
def push():
    """
    approach block then push it one tile ahead and return to initial position
    """
    global idle
    idl_start = time.time()
    ctl.wait_for_bump('right')
    idle += time.time()-idl_start

    global base
    global glob_ori

    drive.on(SpeedRPS(base),SpeedRPS(base))
    # approach block
    light = read_vals()
    case = check_for_block()
    while case != 1:
        case = check_for_block()
        light = read_vals()
        line_correction(light)
    
    # drive for about 30 centimeters to be in middle of next tile
    stop_time = time.time() + 30/(base*17.3)
    drive.on(SpeedRPS(base),SpeedRPS(base))

    while time.time() < stop_time:
        light = read_vals()
        stop_time += line_correction(light)

    drive.off(brake=True)

    drive.on_for_rotations(SpeedRPS(-base), SpeedRPS(-base), 2/17.3)
    drive.on_for_rotations(SpeedRPS(1), SpeedRPS(-1), 1)
    glob_ori = (glob_ori+2)%4

    base = base_slow
    drive.on(SpeedRPS(base),SpeedRPS(base))

    while True:
        light = read_vals()
        if check_for_crossing(light) == 0:
            line_correction(light)
        else:
            base = base_default
            drive.off(brake=True)
            drive.on_for_rotations(SpeedRPS(1), SpeedRPS(1), 4/17.3, brake=True, block=True)
            Sound().beep()
            return


def solve():
    """
    main function of the robot. Takes the time of the driving time and solves the maze
    according to our heuristc.
    """
    begin = time.time()
    red_blocks = []

    global goal
    global glob_ori

    env, straights, start, goal = pf.read_maze()

    ori = pf.d
    case = None

    while case != 0: #If case is zero the abfahrt() function reached the goal
        instruction_dict, path = pf.get_instruction_dict(start, goal, env, ori)

        #push if there is no direct path anymore or if two red blocks are detected.
        if len(red_blocks) == 2 or (not path and len(red_blocks) != 0):
            min_path_len, min_path_start = inf, None
            min_next_pos = None
            min_push_pos = None

            #check all known pushing direction and calculate the best one.
            for push_pos, block_pos in red_blocks:
                d = (block_pos[0]-push_pos[0]),(block_pos[1]-push_pos[1])
                new_block_pos = block_pos[0]+d[0],block_pos[1]+d[1]
                if env[new_block_pos[0]][new_block_pos[1]] == 1:
                    continue
                    
                env[new_block_pos[0]][new_block_pos[1]] = 1
                env[block_pos[0]][block_pos[1]] = 0
                
                path = pf.a_star(block_pos, goal, env)
                
                env[new_block_pos[0]][new_block_pos[1]] = 0
                env[block_pos[0]][block_pos[1]] = 1
                
                if path and min_path_len > len(path):
                    min_path_len = len(path)
                    min_path_start = block_pos
                    min_next_pos = new_block_pos
                    min_push_pos = push_pos
            
            #push block and set the new block position to as wall.
            instruction_dict, path = pf.get_instruction_dict(last_pos, min_push_pos, env, ori)
            _, block_pos = abfahrt(instruction_dict, path, straights, end=min_push_pos)

            d = ((min_path_start[0]-min_push_pos[0])//2, (min_path_start[1]-min_push_pos[1])//2)
            rot = (pf.two_moves_to_rotation[(pf.moves[glob_ori], d)])
            rotate(rot)

            push()

            ori = pf.moves[glob_ori]

            env[min_next_pos[0]][min_next_pos[1]] = 1
            env[min_path_start[0]][min_path_start[1]] = 0
                
            start = min_path_start
            red_blocks = []
            continue

        case, block_pos = abfahrt(instruction_dict, path, straights, end=goal)

        if case == 0:
            break

        #calculate current orientation
        ori = ((last_pos[0]-block_pos[0])//2,(last_pos[1]-block_pos[1])//2)

        drive.off(brake=True)
        if case == 1:
            red_blocks.append((last_pos, block_pos))
            env[block_pos[0]][block_pos[1]] = 1
            start = last_pos
        elif case == 2: #blue
            env[block_pos[0]][block_pos[1]] = 1
            start = last_pos

    drive.off(brake=True)
    driving_time = str(int(100*(time.time()-begin-idle))/100)

    print(driving_time)
    with open('time', 'w') as f:
        f.write(driving_time)

    Sound().play_file('win.wav')
    Sound().speak(driving_time)

ask_for_cal()
solve()